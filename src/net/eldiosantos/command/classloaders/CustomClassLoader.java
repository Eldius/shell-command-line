package net.eldiosantos.command.classloaders;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;

public class CustomClassLoader extends URLClassLoader {

	public CustomClassLoader(URL[] urls) {
		super(urls);
	}

	public CustomClassLoader() {
		super(new URL[0]);
	}

	public Map<String, List<Class<?>>> loadAndScanJar(File jarFile)
			throws ClassNotFoundException, ZipException, IOException {

		System.out.println("Loading package " + jarFile.getPath());

		// Load the jar file into the JVM
		// You can remove this if the jar file already loaded.
		super.addURL(jarFile.toURI().toURL());

		return scanJar(jarFile);
	}

	public Map<String, List<Class<?>>> scanJar(File jarFile) throws IOException {
		Map<String, List<Class<?>>> classes = new HashMap<String, List<Class<?>>>();

		List<Class<?>> interfaces = new ArrayList<Class<?>>();
		List<Class<?>> clazzes = new ArrayList<Class<?>>();
		List<Class<?>> enums = new ArrayList<Class<?>>();
		List<Class<?>> annotations = new ArrayList<Class<?>>();

		classes.put("interfaces", interfaces);
		classes.put("classes", clazzes);
		classes.put("annotations", annotations);
		classes.put("enums", enums);

		// Count the classes loaded
		int count = 0;

		// Your jar file
		JarFile jar = new JarFile(jarFile);
		// Getting the files into the jar
		Enumeration<? extends JarEntry> enumeration = jar.entries();

		// Iterates into the files in the jar file
		while (enumeration.hasMoreElements()) {
			ZipEntry zipEntry = enumeration.nextElement();

			List<String> errorClassNames = new ArrayList<String>();

			// Is this a class?
			if (zipEntry.getName().endsWith(".class")) {

				String className = getClassName(zipEntry);

				Class<?> clazz = getClass(errorClassNames, className);

				count = classifClass(interfaces, clazzes, enums, annotations,
						count, clazz);
			}

			for (String className : errorClassNames) {
				Class<?> clazz = null;

				if ("org.apache.log4j.net.JMSSink".equals(className)) {
					System.out.println("Classe problemática.");
				}

				try {
					clazz = this.loadClass(className);
				} catch (Exception e) {
					System.out.println("Erro ao carregar local.");
					// e.printStackTrace();
					// errorClassNames.add(className);
				} catch (Error e) {
					System.out.println("Erro ao carregar local.");
					// e.printStackTrace();
					// errorClassNames.add(className);
				}

				try {

					count = classifClass(interfaces, clazzes, enums,
							annotations, count, clazz);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		System.out.println("Total: " + count);

		return classes;
	}

	public List<Class<?>> loadAndScanJar(Class<?> clazz, File jarFile)
			throws ClassNotFoundException, ZipException, IOException {

		System.out.println("Loading package " + jarFile.getPath());

		// Load the jar file into the JVM
		// You can remove this if the jar file already loaded.
		super.addURL(jarFile.toURI().toURL());

		return scanJar(clazz, jarFile);
	}

	public List<Class<?>> scanJar(Class<?> clazz, File jarFile)
			throws IOException, ClassNotFoundException {
		List<Class<?>> classes = new ArrayList<Class<?>>();

		// Count the classes loaded
		int count = 0;

		// Your jar file
		JarFile jar = new JarFile(jarFile);
		// Getting the files into the jar
		Enumeration<? extends JarEntry> enumeration = jar.entries();

		// Iterates into the files in the jar file
		while (enumeration.hasMoreElements()) {
			ZipEntry zipEntry = enumeration.nextElement();

			// Is this a class?
			if (zipEntry.getName().endsWith(".class")) {

				String className = getClassName(zipEntry);

				Class<?> c = Class.forName(className);

				for (Class<?> i : c.getInterfaces()) {
					if (i == clazz) {
						classes.add(c);
						break;
					}
				}
			}

		}

		System.out.println("Total: " + count);

		return classes;
	}

	private int classifClass(List<Class<?>> interfaces, List<Class<?>> clazzes,
			List<Class<?>> enums, List<Class<?>> annotations, int count,
			Class<?> clazz) {
		if (clazz != null) {
			try {
				// Verify the type of the "class"
				if (clazz.isInterface()) {
					interfaces.add(clazz);
				} else if (clazz.isAnnotation()) {
					annotations.add(clazz);
				} else if (clazz.isEnum()) {
					enums.add(clazz);
				} else {
					clazzes.add(clazz);
				}

				count++;
			} catch (ClassCastException e) {
				e.printStackTrace();
			}
		}
		return count;
	}

	private Class<?> getClass(List<String> errorClassNames, String className) {
		// Load class definition from JVM
		Class<?> clazz = null;
		try {
			clazz = this.loadClass(className);
		} catch (Exception e) {
			System.out.println("Erro ao carregar local.");
			// e.printStackTrace();
			errorClassNames.add(className);
		} catch (Error e) {
			System.out.println("Erro ao carregar local.");
			// e.printStackTrace();
			errorClassNames.add(className);
		}
		return clazz;
	}

	private String getClassName(ZipEntry zipEntry) {
		// Relative path of file into the jar.
		String className = zipEntry.getName();

		// Complete class name
		className = className.replace(".class", "").replace("/", ".");

		System.out.println("Loading class " + className);
		return className;
	}
}
