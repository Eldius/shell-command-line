package net.eldiosantos.command.commands;

import java.io.File;

import net.eldiosantos.command.commands.interfaces.Command;

/**
 * Base class for the command implementation.
 * 
 * @author Eldius
 * 
 */
public abstract class AbstractCommand implements Command {

	/**
	 * Holds the actual path.
	 */
	protected File path;

	/**
	 * 
	 * @param path
	 *            The actual path
	 */
	public AbstractCommand(File path) {
		super();
		this.path = path;
	}

	// public AbstractCommand() {
	// super();
	// }

	@Override
	public File getPath() {
		return this.path;
	}

	@Override
	public String getCommandName() {
		return this.getClass().getSimpleName().toLowerCase();
	}
}
