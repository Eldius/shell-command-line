package net.eldiosantos.command.commands;

import java.io.File;
import java.io.FilenameFilter;
import java.util.List;
import java.util.Scanner;
import java.util.Vector;

import net.eldiosantos.command.commands.annotations.CommandDef;
import net.eldiosantos.command.commands.response.Response;

/**
 * View the content of a file.
 * 
 * @author Eldius
 * 
 */
@CommandDef
public class Cat extends AbstractCommand {

	public Cat(File path) {
		super(path);
	}

	// public Cat() {
	// super();
	// }

	@Override
	public Response execCommand(String[] params) {
		Response response = null;
		try {
			List<String> lines = new Vector<String>();

			System.out.println(path.getAbsolutePath() + File.separator
					+ params[1]);

			File file = new File(path.getAbsolutePath() + File.separator
					+ params[1]);
			Scanner sc = new Scanner(file);
			while (sc.hasNext()) {
				lines.add(sc.nextLine() + "\n");
			}

			response = new Response(lines, null);
		} catch (Exception e) {
			response = new Response(null, e);
		}
		return response;
	}

	class FilenameFilterByName implements FilenameFilter {

		private final String fileName;

		public FilenameFilterByName(String fileName) {
			super();
			this.fileName = fileName;
		}

		@Override
		public boolean accept(File dir, String name) {

			return (name.equalsIgnoreCase(this.fileName));
		}
	}

	@Override
	public String help() {
		// TODO Auto-generated method stub
		return null;
	}
}
