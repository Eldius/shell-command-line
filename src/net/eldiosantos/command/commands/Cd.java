package net.eldiosantos.command.commands;

import java.io.File;
import java.util.List;
import java.util.Vector;

import net.eldiosantos.command.commands.annotations.CommandDef;
import net.eldiosantos.command.commands.response.Response;

/**
 * Change the actual directory/path.
 * 
 * @author Eldius
 * 
 */
@CommandDef
public class Cd extends AbstractCommand {

	public Cd(File path) {
		super(path);
	}

	// public Cd() {
	// super();
	// }

	@Override
	public Response execCommand(String[] params) {
		// TODO Add change to a specific path
		Response response = null;
		System.out.println("Params[1]" + params[1]);
		try {
			// Changing to a parent folder
			if (params[1].equals("..")) {
				path = path.getParentFile();
			} else {
				// Changing to a child path
				File newPath = new File(path.getAbsolutePath() + File.separator
						+ params[1]);

				path = newPath;
			}
			List<String> resp = new Vector<String>();
			resp.add(path.getAbsolutePath());

			response = new Response(resp, null);

		} catch (Exception e) {
			response = new Response(null, e);
		}

		return response;
	}

	@Override
	public String help() {
		// TODO Auto-generated method stub
		return null;
	}
}
