package net.eldiosantos.command.commands;

import java.io.File;
import java.util.List;
import java.util.Vector;

import net.eldiosantos.command.classloaders.CustomClassLoader;
import net.eldiosantos.command.commands.annotations.CommandDef;
import net.eldiosantos.command.commands.interfaces.Command;
import net.eldiosantos.command.commands.response.Response;
import net.eldiosantos.command.commands.util.CommandVault;

/**
 * Command to load jar packages. May be used to improve the project with more
 * commands.
 * 
 * @author Eldius
 * 
 */
@CommandDef
public class Load extends AbstractCommand {

	public Load(File path) {
		super(path);
	}

	// public Load() {
	// super();
	// }

	@SuppressWarnings("unchecked")
	@Override
	public Response execCommand(String[] params) {
		// TODO Test if the loaded classes may be used by another classloader.
		Response response = null;

		CommandVault vault = new CommandVault();

		try {

			CustomClassLoader loader = new CustomClassLoader();
			File file = null;

			for (int i = 1; i < params.length - 1; i++) {
				file = new File(params[i]);
				List<Class<?>> list = loader
						.loadAndScanJar(Command.class, file);

				for (Class<?> clazz : list) {
					vault.addClass((Class<? extends Command>) clazz);
				}

				file = null;
			}

			// Verify this.

			List<String> lines = new Vector<String>();
			lines.add("All jars loaded.");
			response = new Response(lines, null);

		} catch (Exception e) {
			// TODO: handle exception
			response = new Response(null, e);
		}
		return response;
	}

	@Override
	public String help() {
		// TODO Auto-generated method stub
		return null;
	}

}
