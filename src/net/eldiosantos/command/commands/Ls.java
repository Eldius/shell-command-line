package net.eldiosantos.command.commands;

import java.io.File;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Vector;

import net.eldiosantos.command.commands.annotations.CommandDef;
import net.eldiosantos.command.commands.response.Response;

/**
 * Shows the files and directories above the actual directory.
 * 
 * @author Eldius
 * 
 */
@CommandDef
public class Ls extends AbstractCommand {

	public Ls(File path) {
		super(path);
	}

	// public Ls() {
	// super();
	// }

	@Override
	public Response execCommand(String[] params) {
		// TODO Implement search for a specific file and add support to search
		// params

		Response response = null;
		try {
			List<String> lines = new Vector<String>();

			lines.add(path.getAbsolutePath());

			List<File> tmp = Arrays.asList(super.getPath().listFiles());

			Collections.sort(tmp, new Comparator<File>() {

				@Override
				public int compare(File o1, File o2) {

					return o1.getName().compareToIgnoreCase(o2.getName());
				}
			});

			Collections.sort(tmp, new Comparator<File>() {

				@Override
				public int compare(File o1, File o2) {

					return o1.isDirectory() && !o2.isDirectory() ? -1 : 1;
				}
			});

			for (File f : tmp) {

				StringBuffer sb = new StringBuffer();
				if (f.isDirectory()) {
					sb.append("Directory");
				} else {
					sb.append("         ");
				}

				sb.append("\t" + f.getName() + "\t");

				lines.add(sb.toString());
			}

			response = new Response(lines, null);
		} catch (Exception e) {
			response = new Response(null, e);
		}

		return response;
	}

	@Override
	public String help() {
		// TODO Auto-generated method stub
		return null;
	}
}
