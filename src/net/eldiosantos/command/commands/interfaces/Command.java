package net.eldiosantos.command.commands.interfaces;

import java.io.File;

import net.eldiosantos.command.commands.response.Response;

/**
 * Interface that defines the commands structure.
 * 
 * @author Eldius
 * 
 */
public interface Command {

	/**
	 * Get the actual path from the command after its execution.
	 * 
	 * @return {@link File} Represents the path after the command execution.
	 */
	public abstract File getPath();

	/**
	 * Method to execute the command. Return a Response object with the response
	 * lines and/or the exceptions ocurreds.
	 * 
	 * @param params
	 * @return {@link Response}
	 */
	public abstract Response execCommand(String[] params);

	/**
	 * Returns the String that calls this command.
	 * 
	 * @return {@link String} commandName
	 */
	public abstract String getCommandName();

	/**
	 * Returns the help text about this command.
	 * 
	 * @return {@link String} command help
	 */
	public abstract String help();

}
