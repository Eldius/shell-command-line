package net.eldiosantos.command.commands.response;

import java.io.Serializable;
import java.util.List;
import java.util.Vector;

/**
 * Class to hold the response lines and exceptions ocurreds during the execution
 * process.
 * 
 * @author Eldius
 * 
 */
public class Response implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Lines returned from the command
	 */
	private final List<String> lines;
	private final Exception exception;

	public Response(List<String> lines, Exception exception) {
		super();
		if (lines != null) {
			this.lines = lines;
		} else {
			this.lines = new Vector<String>();
		}
		this.exception = exception;
	}

	public List<String> getLines() {
		return lines;
	}

	/**
	 * 
	 * @return {@link Exception} Exception occurred during the command execution
	 *         or null if no exception.
	 */
	public Exception getException() {
		return exception;
	}

	/**
	 * Verifies the success of the execution.
	 * 
	 * @return {@link Boolean}
	 */
	public boolean isOk() {
		return (this.exception == null);
	}
}
