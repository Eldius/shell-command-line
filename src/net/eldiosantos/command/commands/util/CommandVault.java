package net.eldiosantos.command.commands.util;

import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import net.eldiosantos.command.commands.interfaces.Command;

import org.reflections.Reflections;

/**
 * Class to manage the commands
 * 
 * @author Eldius
 * 
 */
public class CommandVault {

	/**
	 * A Map with all commands and the strings to call them.
	 */
	private static Map<String, Class<? extends Command>> vault;

	public CommandVault() {
		if ((vault == null) || (vault.isEmpty())) {
			try {

				System.out.println("Starting command scan...");

				vault = new HashMap<String, Class<? extends Command>>();

				Reflections reflections = new Reflections("net.eldiosantos");

				Set<Class<? extends Command>> subTypes = reflections
						.getSubTypesOf(Command.class);

				for (Class<? extends Command> c : subTypes) {
					if (!Modifier.isAbstract(c.getModifiers())) {
						addClass(c);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Add a class/command to the system.
	 * 
	 * @param c
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 * @throws NoSuchMethodException
	 */
	public void addClass(Class<? extends Command> c)
			throws InstantiationException, IllegalAccessException,
			InvocationTargetException, NoSuchMethodException {

		System.out.println("Adding class " + c.getName());
		Constructor<? extends Command> constructor = c
				.getConstructor(File.class);
		System.out.println(constructor);
		Command newInstance = constructor.newInstance(new File("."));
		System.out.println(newInstance);
		String cmdName = newInstance.getCommandName();
		vault.put(cmdName, c);
	}

	/**
	 * Returns a class of the string comand.
	 * 
	 * @param cmd
	 *            Command name.
	 * @return A {@link Command} class.
	 */
	public Class<? extends Command> getCommandClass(String cmd) {
		return vault.get(cmd);
	}

	/**
	 * Returns an object that represents the string comand implementation.
	 * 
	 * @param cmd
	 * @param param
	 * @return {@link Command}
	 * @throws IllegalArgumentException
	 * @throws SecurityException
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 * @throws NoSuchMethodException
	 */
	public Command getCommandObject(String cmd, Object param)
			throws IllegalArgumentException, SecurityException,
			InstantiationException, IllegalAccessException,
			InvocationTargetException, NoSuchMethodException {

		Class<? extends Command> clazz = vault.get(cmd);
		Command command = clazz.getConstructor(param.getClass()).newInstance(
				param);
		return command;
	}

}
