package net.eldiosantos.command.core;

import java.io.File;

import net.eldiosantos.command.commands.interfaces.Command;
import net.eldiosantos.command.commands.response.Response;
import net.eldiosantos.command.commands.util.CommandVault;

/**
 * The class responsible to call and execute the commands with the params.
 * 
 * @author Eldius
 * 
 */
public class Interpreter {

	private CommandVault vault = new CommandVault();

	/**
	 * Actual path of the system.
	 */
	private File path;

	/**
	 * Constructor defining the startup path.
	 * 
	 * @param path
	 */
	public Interpreter(File path) {
		super();
		this.path = path;
	}

	/**
	 * Constructor whithout the startup path (defines startup path to the root
	 * folder).
	 */
	public Interpreter() {
		path = new File("/");
	}

	/**
	 * Method to parse and execute the command {@link String}.
	 * 
	 * @param command
	 *            {@link String} Command.
	 */
	public void parseCommand(String command) {

		String tokens[] = command.split(" ");
		try {

			Command cmd = vault.getCommandObject(tokens[0], path);
			Response response = cmd.execCommand(tokens);

			this.path = cmd.getPath();

			if (response.isOk()) {
				for (String string : response.getLines()) {
					System.out.println(string);
				}
			} else {
				System.out.println("Exception raised...");
				System.out.println(response.getException().getMessage());
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
