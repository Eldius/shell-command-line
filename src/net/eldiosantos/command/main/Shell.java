package net.eldiosantos.command.main;

import java.io.File;
import java.util.Scanner;

import net.eldiosantos.command.core.Interpreter;

public class Shell {

	public static void main(String[] args) {
		String cmd;

		Scanner sc = new Scanner(System.in);

		Interpreter interpreter = new Interpreter(new File("/"));

		System.out.println("Console started...");

		do {
			System.out.print("shell: ");
			cmd = sc.nextLine();

			interpreter.parseCommand(cmd);

		} while (!cmd.equals("exit"));
	}
}
